﻿namespace CSharpRomanCalculator
{
  public class CalculationEntryFactory : ICalculationEntryFactory
  {
    public CalculationEntry Create(string leftOperand, string rightOperand, string selectedOperation, string result)
    {
      var entry = new CalculationEntry
      {
        LeftOperand = leftOperand,
        RightOperand = rightOperand,
        Operation = selectedOperation,
        Result = result
      };
      return entry;
    }
  }
}