﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class CalculationEntryCollectionTests
  {
    [TestMethod]
    public void ListContainsCalculationEntriesPassedInConstructorAsList()
    {
      IEnumerable<CalculationEntry> entries = new List<CalculationEntry>()
      {
        new CalculationEntry() {LeftOperand = "left" },
        new CalculationEntry() {LeftOperand = "left", RightOperand = "right"},
        new CalculationEntry() {LeftOperand = "f" }
      };

      var collection = new CalculationEntryCollection(entries);

      CollectionAssert.AreEquivalent(entries.ToList(), collection.List);
    }


  }
}
