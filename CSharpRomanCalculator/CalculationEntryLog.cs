﻿using System.Collections.ObjectModel;

namespace CSharpRomanCalculator
{
  public class CalculationEntryLog : ICalculationEntryLog
  {
    public ObservableCollection<CalculationEntry> EntryLog { get; private set; }
    private readonly IRepository<CalculationEntry> _repository;

    public CalculationEntryLog(IRepository<CalculationEntry> repository, ObservableCollection<CalculationEntry> entryLog)
    {
      EntryLog = entryLog;
      _repository = repository;
    }
    public void Add(CalculationEntry entry)
    {
      EntryLog.Add(entry);
    }

    public void Save()
    {
      _repository.Save(EntryLog);
    }
  }
}