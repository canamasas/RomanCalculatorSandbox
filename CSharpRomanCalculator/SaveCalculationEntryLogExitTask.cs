﻿namespace CSharpRomanCalculator
{
  public class SaveCalculationEntryLogExitTask : IExitTask
  {
    private readonly ICalculationEntryLog _log;

    public SaveCalculationEntryLogExitTask(ICalculationEntryLog log)
    {
      _log = log;
    }

    public void Run()
    {
      _log.Save();
    }
  }
}