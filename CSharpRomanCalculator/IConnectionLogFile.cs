﻿namespace CSharpRomanCalculator
{
  public interface IConnectionLogFile
  {
    string GetPath();
  }
}