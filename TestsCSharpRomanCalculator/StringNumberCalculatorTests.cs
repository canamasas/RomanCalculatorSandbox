﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class StringNumberCalculatorTests 
  {
    [TestMethod]
    public void Addition()
    {
      Mock<IStringNumToIntConverter> converter = new Mock<IStringNumToIntConverter>();
      var calculator = new StringNumberCalculator(converter.Object);

      converter.Setup(c => c.Convert("1")).Returns(1);
      converter.Setup(c => c.Convert("1")).Returns(1);
      converter.Setup(c => c.ConvertBack(2)).Returns("2");

      var result = calculator.Add("1", "1");

      Assert.AreEqual("2", result);
    }

    [TestMethod]
    public void Substraction()
    {
      Mock<IStringNumToIntConverter> converter = new Mock<IStringNumToIntConverter>();
      var calculator = new StringNumberCalculator(converter.Object);

      converter.Setup(c => c.Convert("2")).Returns(2);
      converter.Setup(c => c.Convert("1")).Returns(1);
      converter.Setup(c => c.ConvertBack(1)).Returns("1");

      var result = calculator.Substract("2", "1");

      Assert.AreEqual("1", result);
    }

    [TestMethod]
    public void Multiply()
    {
      Mock<IStringNumToIntConverter> converter = new Mock<IStringNumToIntConverter>();
      var calculator = new StringNumberCalculator(converter.Object);

      converter.Setup(c => c.Convert("2")).Returns(2);
      converter.Setup(c => c.Convert("2")).Returns(2);
      converter.Setup(c => c.ConvertBack(4)).Returns("4");

      var result = calculator.Multiply("2", "2");

      Assert.AreEqual("4", result);
    }

    [TestMethod]
    public void Divide()
    {
      Mock<IStringNumToIntConverter> converter = new Mock<IStringNumToIntConverter>();
      var calculator = new StringNumberCalculator(converter.Object);

      converter.Setup(c => c.Convert("4")).Returns(4);
      converter.Setup(c => c.Convert("2")).Returns(2);
      converter.Setup(c => c.ConvertBack(2)).Returns("2");

      var result = calculator.Divide("4", "2");

      Assert.AreEqual("2", result);
    }
  }
}
