﻿using System.Collections.ObjectModel;

namespace CSharpRomanCalculator
{
  public class CalculationEntryLogFactory : ICalculationEntryLogFactory
  {
    private readonly IRepository<CalculationEntry> _storage;

    public CalculationEntryLogFactory(IRepository<CalculationEntry> storage)
    {
      _storage = storage;
    }

    public CalculationEntryLog CreateLog()
    {
      var entryLogList = _storage.GetAll();
      var entryLog = new ObservableCollection<CalculationEntry>(entryLogList);
      var calculationEntryLog = new CalculationEntryLog(_storage, entryLog);

      return calculationEntryLog;
    }
  }
}