﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class CalculationEntrySerializerTests
  {
    [TestMethod]
    public void SerializeAndDeserializeSuccess()
    {
      var serializer = new CalculationEntrySerializer();
      IEnumerable<CalculationEntry> entries = new List<CalculationEntry>() {new CalculationEntry() {LeftOperand = "l", RightOperand = "r", Operation = "o", Result = "r"} };
      IEnumerable<CalculationEntry> newEntries;

      using (var stream = new MemoryStream())
      {
        serializer.Serialize(stream, entries);
        stream.Position = 0;
        newEntries = serializer.Deserialize(stream);
      }
      VerifyEntries(entries.Single(), newEntries.Single());
    }

    private static void VerifyEntries(CalculationEntry expectedEntry, CalculationEntry newEntry)
    {
      Assert.AreEqual(expectedEntry.LeftOperand, newEntry.LeftOperand);
      Assert.AreEqual(expectedEntry.RightOperand, newEntry.RightOperand);
      Assert.AreEqual(expectedEntry.Operation, newEntry.Operation);
      Assert.AreEqual(expectedEntry.Result, newEntry.Result);
    }

    [TestMethod]
    public void DeserializeEmptyStreamReturnsEmptyList()
    {
      var serializer = new CalculationEntrySerializer();
      IEnumerable<CalculationEntry> entries = new List<CalculationEntry>();
      IEnumerable<CalculationEntry> newEntries;

      using (var stream = new MemoryStream())
      {
        serializer.Serialize(stream, entries);
        stream.Position = 0;
        newEntries = serializer.Deserialize(stream);
      }

      CollectionAssert.AreEquivalent(new List<CalculationEntry>(), newEntries.ToList());
    }
  }
}
