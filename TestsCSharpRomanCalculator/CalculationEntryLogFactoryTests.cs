﻿using System;
using System.Collections.Generic;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class CalculationEntryLogFactoryTests
  {
    private Mock<IRepository<CalculationEntry>> _storage;
    private CalculationEntryLogFactory _factory;

    [TestInitialize]
    public void Setup()
    {
      _storage = new Mock<IRepository<CalculationEntry>>();
      _factory = new CalculationEntryLogFactory(_storage.Object);
    }

    [TestMethod]
    public void ReturnsAnEmptyCalculationEntryLogObjectIfStorageIsEmpty()
    {
      var expectedList = new List<CalculationEntry>();
      _storage.Setup(s => s.GetAll()).Returns(expectedList);

      var log = _factory.CreateLog();

      CollectionAssert.AreEquivalent(expectedList, log.EntryLog);
    }

    [TestMethod]
    public void ReturnsCalculationEntryLogWithEntryLogEquivalentToWhatIsInStorage()
    {
      var expectedList = new List<CalculationEntry>()
      {
        new CalculationEntry() {LeftOperand = "blah" },
        new CalculationEntry() {RightOperand = "bloh"}
      };

      _storage.Setup(s => s.GetAll()).Returns(expectedList);

      var log = _factory.CreateLog();

      CollectionAssert.AreEquivalent(expectedList, log.EntryLog);
    }

  }
}
