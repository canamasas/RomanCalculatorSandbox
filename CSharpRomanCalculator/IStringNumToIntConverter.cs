﻿namespace CSharpRomanCalculator
{
  public interface IStringNumToIntConverter
  {
    int Convert(string romanNumber);
    string ConvertBack(int number);
  }
}