﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class SaveCalculationEntryLogExitTaskTest
  {
    [TestMethod]
    public void WhenRunSavesTheLog()
    {
      Mock<ICalculationEntryLog> log = new Mock<ICalculationEntryLog>();
      var saverOnExit = new SaveCalculationEntryLogExitTask(log.Object);

      saverOnExit.Run();

      log.Verify(l => l.Save());
    }
  }
}
