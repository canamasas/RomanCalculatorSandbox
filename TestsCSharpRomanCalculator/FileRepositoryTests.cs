﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class FileRepositoryTests
  {
    private FileRepository _storage;
    private CalculationEntry _calculationEntry;
    private Mock<IFileSystem> _fileSystem;
    private Mock<ICalculationEntrySerializer> _serializer;
    private IEnumerable<CalculationEntry> _entries;
    private IConnectionLogFile _connectionLogFile;

    [TestInitialize]
    public void Setup()
    {
      _calculationEntry = new CalculationEntry()
      {
        LeftOperand = "left",
        RightOperand = "right",
        Operation = "operation",
        Result = "result"
      };

      _entries = new List<CalculationEntry>() {_calculationEntry}; 

      _connectionLogFile = new ConnectionLogFile();
      _fileSystem = new Mock<IFileSystem>();
      _serializer = new Mock<ICalculationEntrySerializer>();
      _storage = new FileRepository(_connectionLogFile, _fileSystem.Object, _serializer.Object);
    }

    [TestMethod]
    public void SaveSerializesTheList()
    {
      var stream = new MemoryStream();
      _fileSystem.Setup(f => f.Open(_connectionLogFile.GetPath(), FileMode.Create)).Returns(stream);

      _storage.Save(_entries);

      _serializer.Verify(s => s.Serialize(stream, _entries));
    }

    [TestMethod]
    public void SaveOpensConnectionLogFilePath()
    {
      _storage.Save(_entries);

      _fileSystem.Verify(f => f.Open(_connectionLogFile.GetPath(), FileMode.Create));
    }

    [TestMethod]
    public void GetAllGetsAllRowsIfFound()
    {
      var stream = new MemoryStream();
      _fileSystem.Setup(f => f.Open(_connectionLogFile.GetPath(), FileMode.Open)).Returns(stream);
      _serializer.Setup(s => s.Deserialize(stream)).Returns(_entries);

      var entriesReturned = _storage.GetAll();

      CollectionAssert.AreEqual(_entries.ToList(), entriesReturned.ToList());
    }

    [TestMethod]
    public void GetOpensFilePathOfConnectionLogFile()
    {
      var entriesReturned = _storage.GetAll();

      _fileSystem.Verify(f => f.Open(_connectionLogFile.GetPath(), FileMode.Open));
    }

    [TestMethod]
    public void GetReturnsEmptyListIfUnableToOpenConnectionLogFile()
    {
      _fileSystem.Setup(f => f.Open(_connectionLogFile.GetPath(), FileMode.Open)).Throws<FileNotFoundException>();

      var entriesReturned = _storage.GetAll();

      CollectionAssert.AreEqual(new List<CalculationEntry>(), entriesReturned.ToList());
    }
  }
}
