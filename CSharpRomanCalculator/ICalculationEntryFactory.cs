﻿namespace CSharpRomanCalculator
{
  public interface ICalculationEntryFactory
  {
    CalculationEntry Create(string leftOperand, string rightOperand, string selectedOperation, string result);
  }
}