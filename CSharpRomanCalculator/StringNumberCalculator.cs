﻿namespace CSharpRomanCalculator
{
  public class StringNumberCalculator : IStringNumberCalculator
  {
    private readonly IStringNumToIntConverter _converter;

    public StringNumberCalculator(IStringNumToIntConverter converter)
    {
      _converter = converter;
    }

    public string Add(string leftOperand, string rightOperand)
    {
      var left = _converter.Convert(leftOperand);
      var right = _converter.Convert(rightOperand);

      var result = left + right;

      return _converter.ConvertBack(result);
    }

    public string Substract(string leftOperand, string rightOperand)
    {
      var left = _converter.Convert(leftOperand);
      var right = _converter.Convert(rightOperand);

      var result = left - right;

      return _converter.ConvertBack(result);
    }

    public string Multiply(string leftOperand, string rightOperand)
    {
      var left = _converter.Convert(leftOperand);
      var right = _converter.Convert(rightOperand);

      var result = left * right;

      return _converter.ConvertBack(result);
    }

    public string Divide(string leftOperand, string rightOperand)
    {
      var left = _converter.Convert(leftOperand);
      var right = _converter.Convert(rightOperand);

      var result = left / right;

      return _converter.ConvertBack(result);
    }
  }
}