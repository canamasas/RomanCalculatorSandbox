﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CSharpRomanCalculator
{
  public class FileRepository : IRepository<CalculationEntry>
  {
    private readonly IConnectionLogFile _connectionLogFile;
    private readonly IFileSystem _fileSystem;
    private readonly ICalculationEntrySerializer _serializer;

    public FileRepository(IConnectionLogFile connectionLogFile, IFileSystem fileSystem,
      ICalculationEntrySerializer serializer)
    {
      _connectionLogFile = connectionLogFile;
      _fileSystem = fileSystem;
      _serializer = serializer;
    }

    public void Save(IEnumerable<CalculationEntry> entries)
    {
      string filePath = _connectionLogFile.GetPath();
      using (Stream stream = _fileSystem.Open(filePath, FileMode.Create))
      {
        _serializer.Serialize(stream, entries);
        //TODO: Check for InvalidOperationException
      }
    }

    public IEnumerable<CalculationEntry> GetAll()
    {
      string filePath = _connectionLogFile.GetPath();
      Stream stream = null;
      try
      {
        stream = _fileSystem.Open(filePath, FileMode.Open);

        return _serializer.Deserialize(stream);

        //TODO: Check for InvalidOperationException
      }
      catch (FileNotFoundException)
      {
        return new List<CalculationEntry>();
      }
      finally
      {
        if (stream != null)
        {
          stream.Dispose();
        }
      }
    }
  }
}