﻿namespace CSharpRomanCalculator
{
  public interface IStringNumberCalculator
  {
    string Add(string leftOperand, string rightOperand);
    string Substract(string leftOperand, string rightOperand);
    string Multiply(string leftOperand, string rightOperand);
    string Divide(string leftOperand, string rightOperand);
  }
}