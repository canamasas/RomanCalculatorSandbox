﻿using System.IO;

namespace CSharpRomanCalculator
{
  public class FileSystem : IFileSystem
  {
    public Stream Open(string filePath, FileMode open)
    {
      return File.Open(filePath, open);
    }
}
}