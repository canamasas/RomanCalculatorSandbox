﻿using System;
using System.Collections.ObjectModel;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class CalculationEntryLogTests
  {
    private Mock<IRepository<CalculationEntry>> _repository;
    private ObservableCollection<CalculationEntry> _expectedEntryLog;
    private CalculationEntryLog _log;

    [TestInitialize]
    public void Setup()
    {
      _repository = new Mock<IRepository<CalculationEntry>>();
      _expectedEntryLog = new ObservableCollection<CalculationEntry>() { new CalculationEntry() };
      _log = new CalculationEntryLog(_repository.Object, _expectedEntryLog);
    }

    [TestMethod]
    public void EntryLogIsWhatIsPassedInConstructor()
    {
      Assert.AreSame(_expectedEntryLog, _log.EntryLog);
    }

    [TestMethod]
    public void AddEntryToLogIsAddedToEntryLog()
    {
      var entry = new CalculationEntry() {LeftOperand = "left", RightOperand = "right", Operation = "new"};

      _log.Add(entry);

      CollectionAssert.Contains(_log.EntryLog, entry);
    }

    [TestMethod]
    public void SaveLogEntryIsSavedToRepository()
    {
      _log.Save();

      _repository.Verify(r => r.Save(_log.EntryLog));
    }
  }
}
