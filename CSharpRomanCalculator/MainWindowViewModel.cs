﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CSharpRomanCalculator.Annotations;
using Prism.Commands;
using Prism.Mvvm;

namespace CSharpRomanCalculator
{
  public class MainWindowViewModel : BindableBase
  {
    private string _leftOperand;
    private string _result;
    private string _rightOperand;
    private readonly IStringNumberCalculator _calculator;
    private readonly ICalculationEntryLog _log;
    private readonly ICalculationEntryFactory _calculationEntryFactory;
    private string _errorMessage;

    public MainWindowViewModel(IStringNumberCalculator calculator, ICalculationEntryLog log,
      ICalculationEntryFactory calculationEntryFactory)
    {
      _log = log;
      _calculator = calculator;
      _calculationEntryFactory = calculationEntryFactory;
      ClearFieldsCommand = new DelegateCommand(ClearFields);
      CalculateCommand = new DelegateCommand(Calculate);
    }

    public string LeftOperand
    {
      get { return _leftOperand; }
      set
      {
        SetProperty(ref _leftOperand, value);
      }
    }

    public string RightOperand
    {
      get { return _rightOperand; }
      set
      {
        SetProperty(ref _rightOperand, value);
      }
    }

    public string Result
    {
      get { return _result; }
      private set
      {
        SetProperty(ref _result, value);
      }
    }

    public string ErrorMessage
    {
      get { return _errorMessage; }
      private set
      {
        SetProperty(ref _errorMessage, value);
      }
    }

    public DelegateCommand ClearFieldsCommand { get; }

    public DelegateCommand CalculateCommand { get; }

    public ICalculationEntryLog EntryLog => _log;

    //TODO: Must be an easier way of doing this!
    public List<Operation> Operations
      =>
      new List<Operation>()
      {
        new Operation() {Name = "+"},
        new Operation() {Name = "-"},
        new Operation() {Name = "*"},
        new Operation() {Name = "/"}
      };

    public string SelectedOperation { get; set; }

    private void ClearFields()
    {
      LeftOperand = "";
      RightOperand = "";
      Result = "";
    }

    private void Calculate()
    {
      try
      {
        Result = DoCalculation();
        var entry = _calculationEntryFactory.Create(LeftOperand, RightOperand, SelectedOperation, Result);
        EntryLog.Add(entry);
        ErrorMessage = "";
      }
      catch (InvalidCastException e)
      {
        ErrorMessage = e.Message;
      }
    }

    private string DoCalculation()
    {
      string result = "";
      if (SelectedOperation == "+")
      {
        result = _calculator.Add(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "-")
      {
        result = _calculator.Substract(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "*")
      {
        result = _calculator.Multiply(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "/")
      {
        result = _calculator.Divide(LeftOperand, RightOperand);
      }

      return result;
    }
  }
}