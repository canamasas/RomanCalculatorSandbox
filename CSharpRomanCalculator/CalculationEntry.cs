﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace CSharpRomanCalculator
{
  [Serializable]
  public class CalculationEntry : ISerializable
  {
    [XmlElement("LeftOperand")]
    public string LeftOperand { get; set; }
    [XmlElement("RightOperand")]
    public string RightOperand { get; set; }
    [XmlElement("Operation")]
    public string Operation { get; set; }
    [XmlElement("Result")]
    public string Result { get; set; }
    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      info.AddValue("LeftOperand", LeftOperand);
      info.AddValue("RightOperand", RightOperand);
      info.AddValue("Operation", Operation);
      info.AddValue("Result", Result);
    }
  }
}