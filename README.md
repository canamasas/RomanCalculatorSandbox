This is a sandbox project, to try and experiment different things.
This is by no means ready, as it has known bugs, and has few TODO's, but I hope it gives a hint of my coding style and philosopy.

My goal when coding is to create code that is maintainable, simple, understandable, expandable and tested.
I believe that the code should be self explanatory, with small functions, minimal dependencies, with minimal comments and with the features just needed for doing the work.
To my eyes, testing coverage is a key ingredient to this, and I try to design the code so that it can be easily tested.

This example is simply a Roman Calculator. It is a simple problem, but I have overcomplicated a bit, as I use this as a sandbox for practice. 
I have added a bootstrapper from Ninject and used Prism. This added me the functionality for separating the building and the execution of the program. As well, it helped in breaking the dependencies between the views and the viewmodels.
The code behind is almost empty, so all functionality can be easily tested via Unit Tests.

The program is by no means ready, it is the result of 3 evenings of coding.

The main areas that lack are:

-Usage of Data Driven tests

-Error handling

-Views have not received much care.

-Conversion from Roman To Arabic is not finished, there are bugs in the validation.

Bruno