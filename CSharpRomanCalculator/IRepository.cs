﻿using System.Collections.Generic;

namespace CSharpRomanCalculator
{
  public interface IRepository<T>
  {
    IEnumerable<T> GetAll();
    void Save(IEnumerable<CalculationEntry> entries);
  }
}