﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsCSharpRomanCalculator
{
  //TODO: Use Data Driven Tests

  [TestClass]
  public class RomanToIntTests
  {
    private RomanToInt _converter;

    [TestInitialize]
    public void Setup()
    {
      _converter = new RomanToInt();
    }
    [TestMethod]
    public void I()
    {
      var result = _converter.Convert("I");

      Assert.AreEqual(1, result);
    }

    [TestMethod]
    public void One()
    {
      var result = _converter.ConvertBack(1);

      Assert.AreEqual("I", result);
    }

    [TestMethod]
    public void II()
    {
      var result = _converter.Convert("II");

      Assert.AreEqual(2, result);
    }

    [TestMethod]
    public void Two()
    {
      var result = _converter.ConvertBack(2);

      Assert.AreEqual("II", result);
    }

    [TestMethod]
    public void III()
    {
      var result = _converter.Convert("III");

      Assert.AreEqual(3, result);
    }

    [TestMethod]
    public void Three()
    {
      var result = _converter.ConvertBack(3);

      Assert.AreEqual("III", result);
    }

    [TestMethod]
    public void IV()
    {
      var result = _converter.Convert("IV");

      Assert.AreEqual(4, result);
    }

    [TestMethod]
    public void Four()
    {
      var result = _converter.ConvertBack(4);

      Assert.AreEqual("IV", result);
    }

    [TestMethod]
    public void VIII()
    {
      var result = _converter.Convert("VIII");

      Assert.AreEqual(8, result);
    }

    [TestMethod]
    public void Eight()
    {
      var result = _converter.ConvertBack(8);

      Assert.AreEqual("VIII", result);
    }


    [TestMethod]
    public void XXXX()
    {
      var result = _converter.Convert("XXXX");

      Assert.AreEqual(40, result);
    }

    [TestMethod]
    public void XL()
    {
      var result = _converter.Convert("XL");

      Assert.AreEqual(40, result);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void XXXXXisException()
    {
      _converter.Convert("XXXXX");
    }

    [TestMethod]
    public void Fourty()
    {
      var result = _converter.ConvertBack(40);

      Assert.AreEqual("XL", result);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void IllegalStringIsException()
    {
      _converter.Convert("XA");
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void TooLong9999IsException()
    {
      _converter.ConvertBack(9999);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void TooLong5000IsException()
    {
      _converter.ConvertBack(5000);
    }

    [TestMethod]
    public void UpperLimit4999()
    {
      var result = _converter.ConvertBack(4999);

      Assert.AreEqual("MMMMCMXCIX", result);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void ZeroIsException()
    {
      _converter.ConvertBack(0);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    public void NegativeIsException()
    {
      _converter.ConvertBack(-1);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidCastException))]
    [Ignore]
    public void WrongUsageOfM()
    {
      //TODO: There are known bugs in the validation of the roman numbers...
      _converter.Convert("MCXM");
    }
  }
}
