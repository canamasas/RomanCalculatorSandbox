﻿using System.Collections.Generic;
using System.IO;

namespace CSharpRomanCalculator
{
  public interface ICalculationEntrySerializer
  {
    void Serialize(Stream stream, IEnumerable<CalculationEntry> entries);
    IEnumerable<CalculationEntry> Deserialize(Stream stream);
  }
}