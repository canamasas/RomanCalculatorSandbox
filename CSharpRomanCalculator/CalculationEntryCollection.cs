﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace CSharpRomanCalculator
{
  [XmlRoot(ElementName = "CalculationEntries")]
  public class CalculationEntryCollection
  {
    [XmlElement("CalculationEntry")]
    public List<CalculationEntry> List { get; private set; }

    public CalculationEntryCollection()
    {
      List = new List<CalculationEntry>();
    }
    public CalculationEntryCollection(IEnumerable<CalculationEntry> entries)
    {
      List = entries.ToList();
    }
  }
}