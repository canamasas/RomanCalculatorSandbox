﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Ninject;
using Prism;

namespace CSharpRomanCalculator
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    private Bootstrapper _bootstrapper;
    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);

      Dispatcher.UnhandledException += Dispatcher_UnhandledException;

      _bootstrapper = new Bootstrapper();
      _bootstrapper.Run();
    }

    private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
    {
      string message = "Something went wrong" + " :(\n\n" + e.Exception.Message + e.Exception.StackTrace;
      string title = "We've got a problem";

      MessageBox.Show(message, title, MessageBoxButton.OK);
      e.Handled = true;
      Environment.Exit(1);
    }

    protected override void OnExit(ExitEventArgs e)
    {
      IEnumerable<IExitTask> exitTasks = _bootstrapper.Kernel.GetAll<IExitTask>();
      foreach (IExitTask task in exitTasks)
      {
        task.Run();
      }

      base.OnExit(e);
    }
  }
}
