﻿using CSharpRomanCalculator.Properties;

namespace CSharpRomanCalculator
{
  public class ConnectionLogFile : IConnectionLogFile
  {
    public string GetPath()
    {
      return Resources.PathToConnectionLogFile;
    }
  }
}