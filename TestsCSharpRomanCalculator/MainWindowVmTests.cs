﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class MainWindowVmTests
  {
    private Mock<IStringNumberCalculator> _calculator;
    private Mock<ICalculationEntryLog> _log;
    private Mock<ICalculationEntryFactory> _calculationEntryFactory;
    private MainWindowViewModel _viewModel;

    [TestInitialize]
    public void Setup()
    {
      _calculator = new Mock<IStringNumberCalculator>();
      _log = new Mock<ICalculationEntryLog>();
      _calculationEntryFactory = new Mock<ICalculationEntryFactory>();
      _viewModel = new MainWindowViewModel(_calculator.Object, _log.Object, _calculationEntryFactory.Object);
    }

    [TestMethod]
    public void LeftOperandIsEmptyAtStart()
    {
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.LeftOperand));
    }

    [TestMethod]
    public void RightOperandIsEmptyAtStart()
    {
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.RightOperand));
    }

    [TestMethod]
    public void ResultIsEmptyAtStart()
    {
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.Result));
    }

    [TestMethod]
    public void LeftOperandHasSameValueAsAssigned()
    {
      _viewModel.LeftOperand = "II";
      Assert.AreEqual("II", _viewModel.LeftOperand);
    }

    [TestMethod]
    public void RightOperandHasSameValueAsAssigned()
    {
      _viewModel.RightOperand = "II";
      Assert.AreEqual("II", _viewModel.RightOperand);
    }

    [TestMethod]
    public void WhenResultIsModifiedOnPropertyChangedIsRaised()
    {
      bool result = false;
      _viewModel.PropertyChanged += (s, e) =>
      {
        if (e.PropertyName == nameof(_viewModel.Result))
        {
          result = true;
        }
      };
      _viewModel.SelectedOperation = "+";
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Returns("II");

      _viewModel.CalculateCommand.Execute();
      Assert.IsTrue(result);
    }

    [TestMethod]
    public void WhenLeftOperandIsModifiedOnPropertyChangedIsRaised()
    {
      bool result = false;
      _viewModel.PropertyChanged += (s, e) =>
      {
        if (e.PropertyName == nameof(_viewModel.LeftOperand))
        {
          result = true;
        }
      };
      _viewModel.LeftOperand = "changed";

      Assert.IsTrue(result);
    }

    [TestMethod]
    public void WhenRightOperandIsModifiedOnPropertyChangedIsRaised()
    {
      bool result = false;
      _viewModel.PropertyChanged += (s, e) =>
      {
        if (e.PropertyName == nameof(_viewModel.RightOperand))
        {
          result = true;
        }
      };
      _viewModel.RightOperand = "changed";

      Assert.IsTrue(result);
    }

    [TestMethod]
    public void ClearFieldsCommandsSetsLeftOperandToEmpty()
    {
      _viewModel.LeftOperand = "II";
      _viewModel.ClearFieldsCommand.Execute();
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.LeftOperand));
    }

    [TestMethod]
    public void ClearFieldsCommandsSetsRightOperandToEmpty()
    {
      _viewModel.RightOperand = "II";
      _viewModel.ClearFieldsCommand.Execute();
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.RightOperand));
    }

    [TestMethod]
    public void ClearFieldsCommandsSetsResultToEmpty()
    {
      _viewModel.SelectedOperation = "+";
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Returns("II");
      _viewModel.ClearFieldsCommand.Execute();
      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.RightOperand));
    }

    [TestMethod]
    public void ClearFieldsCommandCanExecuteIsFalseWhenLeftOperandIsNotEmpty()
    {
      _viewModel.LeftOperand = "NotEmpty";
      Assert.IsTrue(_viewModel.ClearFieldsCommand.CanExecute());
    }
    [TestMethod]
    public void ClearFieldsCommandCanExecuteIsFalseWhenRightOperandIsNotEmpty()
    {
      _viewModel.RightOperand = "NotEmpty";
      Assert.IsTrue(_viewModel.ClearFieldsCommand.CanExecute());
    }
    [TestMethod]
    public void ClearFieldsCommandCanExecuteIsFalseWhenResultIsNotEmpty()
    {
      _viewModel.SelectedOperation = "+";
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Returns("II");
      _viewModel.CalculateCommand.Execute();

      Assert.IsTrue(_viewModel.ClearFieldsCommand.CanExecute());
    }

    [TestMethod]
    public void WhenAdditionOperationAddIsCalledWithCorrectOperands()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";
      
      _viewModel.CalculateCommand.Execute();
      _calculator.Verify(c => c.Add("1", "2"));
    }

    [TestMethod]
    public void WhenSubstractionOperationSubstractIsCalledWithCorrectOperands()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "-";

      _viewModel.CalculateCommand.Execute();

      _calculator.Verify(c => c.Substract("1", "2"));
    }
    [TestMethod]
    public void WhenMultiplicationOperationMultiplyIsCalledWithCorrectOperands()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "*";

      _viewModel.CalculateCommand.Execute();

      _calculator.Verify(c => c.Multiply("1", "2"));
    }
    [TestMethod]
    public void WhenDivisionOperationDivideIsCalledWithCorrectOperands()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "/";

      _viewModel.CalculateCommand.Execute();

      _calculator.Verify(c => c.Divide("1", "2"));
    }
    [TestMethod]
    public void WhenNoOperationNoOperationIsCalled()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "";

      _viewModel.CalculateCommand.Execute();

      _calculator.Verify(c => c.Add(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
      _calculator.Verify(c => c.Substract(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
      _calculator.Verify(c => c.Multiply(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
      _calculator.Verify(c => c.Divide(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
    }

    [TestMethod]
    public void CalculationEntryIsCreatedWithCorrectParameters()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Returns("II");

      _viewModel.CalculateCommand.Execute();

      _calculationEntryFactory.Verify(f => f.Create("1", "2", "+", "II"));
    }

    [TestMethod]
    public void EntryIsAddedToEntryLog()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";

      var logEntry = new CalculationEntry
      {
        LeftOperand = "1",
        RightOperand = "2",
        Operation = "+",
        Result = "II"
      };

      _calculationEntryFactory.Setup(f => f.Create(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(logEntry);

      _viewModel.CalculateCommand.Execute();

      _log.Verify(l => l.Add(logEntry));
    }

    [TestMethod]
    public void CalculateCommandCanExecuteIsTrueWhenLeftOperandAndRightOperandAndSelectedOperationAreNotEmpty()
    {
      _viewModel.LeftOperand = "2";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";
      Assert.IsTrue(_viewModel.CalculateCommand.CanExecute());
    }

    [TestMethod]
    public void WhenCalculateCommandGeneratesInvalidCastExceptionErrorMessageIsSet()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";

      Exception exception = new InvalidCastException("exception message");
 
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Throws(exception);

      _viewModel.CalculateCommand.Execute();

      Assert.AreEqual("exception message", _viewModel.ErrorMessage);
    }

    [TestMethod]
    public void WhenMessageIsSetOnPropertyChangedEventIsRisen()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";

      bool result = false;
      _viewModel.PropertyChanged += (s, e) =>
      {
        if (e.PropertyName == nameof(_viewModel.ErrorMessage))
        {
          result = true;
        }
      };

      Exception exception = new InvalidCastException("exception message");

      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Throws(exception);

      _viewModel.CalculateCommand.Execute();

      Assert.IsTrue(result);
    }

    [TestMethod]
    public void WhenCalculateCommandGeneratesInvalidCastExceptionNoEntryIsSet()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";

      Exception exception = new InvalidCastException("exception message");

      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Throws(exception);

      _viewModel.CalculateCommand.Execute();

      _log.Verify(l => l.Add(It.IsAny<CalculationEntry>()), Times.Never);
    }

    [TestMethod]
    public void WhenCalculateCommandSucceedsErrorMessageIsEmptied()
    {
      _viewModel.LeftOperand = "1";
      _viewModel.RightOperand = "2";
      _viewModel.SelectedOperation = "+";

      //Failed Execution
      Exception exception = new InvalidCastException("exception message");
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Throws(exception);
      _viewModel.CalculateCommand.Execute();
      Assert.IsFalse(String.IsNullOrEmpty(_viewModel.ErrorMessage));

      //Successful Execution
      _calculator.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<string>())).Returns(It.IsAny<string>());
      _viewModel.CalculateCommand.Execute();

      Assert.IsTrue(String.IsNullOrEmpty(_viewModel.ErrorMessage));
    }
  }
}
