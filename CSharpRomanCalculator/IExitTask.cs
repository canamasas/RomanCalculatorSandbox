﻿namespace CSharpRomanCalculator
{
  public interface IExitTask
  {
    void Run();
  }
}