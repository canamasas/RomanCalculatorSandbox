﻿using System.IO;

namespace CSharpRomanCalculator
{
  public interface IFileSystem
  {
    Stream Open(string filePath, FileMode open);
  }
}