﻿using System.Collections.ObjectModel;

namespace CSharpRomanCalculator
{
  public interface ICalculationEntryLog
  {
    ObservableCollection<CalculationEntry> EntryLog { get; }
    void Add(CalculationEntry entry);
    void Save();
  }
}