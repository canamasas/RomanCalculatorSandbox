﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using CSharpRomanCalculator.Properties;

namespace CSharpRomanCalculator
{
  public class RomanToInt : IStringNumToIntConverter
  {
    private readonly IDictionary<int, string> _arabicToInt = new Dictionary<int, string>
    {
      {3000, "MMM"},
      {2000, "MM"},
      {1000, "M"},
      {900, "CM"},
      {500, "D"},
      {400, "CD"},
      {300, "CCC"},
      {200, "CC"},
      {100, "C"},
      {90, "XC"},
      {50, "L"},
      {40, "XL"},
      {30, "XXX"},
      {20, "XX"},
      {10, "X"},
      {9, "IX"},
      {5, "V"},
      {4, "IV"},
      {3, "III"},
      {2, "II"},
      {1, "I"}
    };

    private readonly IDictionary<string, int> _romanToInt = new Dictionary<string, int>
    {
      {"MMM", 3000},
      {"MM", 2000},
      {"M", 1000},
      {"CM", 900},
      {"D", 500},
      {"CD", 400},
      {"CCC", 300},
      {"CC", 200},
      {"C", 100},
      {"XC", 90},
      {"L", 50},
      {"XL", 40},
      {"XXX", 30 },
      {"XX", 20 },
      {"X", 10 },
      {"IX", 9 },
      {"V", 5 },
      {"IV", 4 },
      {"III", 3 },
      {"II", 2 },
      {"I", 1 }
    };

    private readonly IList<string> _romanToIntLimitRepetitions = new List<string>
    {
      "MMMMM",
      "DD",
      "CCCCC",
      "LL",
      "XXXXX",
      "VV",
      "IIIII"
    };
    public int Convert(string romanNumber)
    {
      var backupRomanNumber = romanNumber;
      var number = 0;
      var previousLength = -1;
      while (romanNumber.Length != 0 && romanNumber.Length != previousLength)
      {
        previousLength = romanNumber.Length;
        foreach (var entry in _romanToInt.Keys)
        {
          //TODO: Check that Bigger values are not repeated. 
          if (TooManyRepeatedRomanNumbers(romanNumber))
          {
            throw new InvalidCastException($"Roman number contains illegal amount of repetitions in number {backupRomanNumber}");
          }
          if (romanNumber.StartsWith(entry))
          {
            romanNumber = romanNumber.Substring(entry.Length);
            number += _romanToInt[entry];
          }
        }
      }
      if (romanNumber.Length > 0)
      {
        throw new InvalidCastException(String.Format(Resources.UnableToConvertRomanNumber, backupRomanNumber));
      }
      return number;
    }

    private bool TooManyRepeatedRomanNumbers(string romanNumber)
    {
      foreach (var limit in _romanToIntLimitRepetitions)
      {
        if (romanNumber.StartsWith(limit))
        {
          return true;
        }
      }
      return false;
    }

    public string ConvertBack(int number)
    {
      var romanNumber = "";

      if ((number > 4999) || (number <= 0))
      {
        throw new InvalidCastException(String.Format(Resources.ErrorUnabletoConvertToNumber, number));
      }
      while (number > 0)
      {
        foreach (var key in _arabicToInt.Keys)
        {
          if (number >= key)
          {
            number -= key;
            romanNumber += _arabicToInt[key];
          }
        }
      }
      return romanNumber;
    }
  }
}