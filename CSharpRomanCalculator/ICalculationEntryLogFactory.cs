﻿namespace CSharpRomanCalculator
{
  public interface ICalculationEntryLogFactory
  {
    CalculationEntryLog CreateLog();
  }
}