﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class CalculationEntryFactoryTest
  {
    [TestMethod]
    public void LeftOperandIsInEntryLeftOperand()
    {
      var factory = new CalculationEntryFactory();

      var expectedResult = "Result";
      var entry = factory.Create(expectedResult, null, null, null);

      Assert.AreEqual(expectedResult, entry.LeftOperand);
    }

    [TestMethod]
    public void RightOperandIsInEntryRightOperand()
    {
      var factory = new CalculationEntryFactory();

      var expectedResult = "Result";
      var entry = factory.Create(null, expectedResult, null, null);

      Assert.AreEqual(expectedResult, entry.RightOperand);
    }

    [TestMethod]
    public void OperationIsInEntryOperation()
    {
      var factory = new CalculationEntryFactory();

      var expectedResult = "Result";
      var entry = factory.Create(null, null, expectedResult, null);

      Assert.AreEqual(expectedResult, entry.Operation);
    }

    [TestMethod]
    public void ResultIsInEntryResult()
    {
      var factory = new CalculationEntryFactory();

      var expectedResult = "Result";
      var entry = factory.Create(null, null, null, expectedResult);

      Assert.AreEqual(expectedResult, entry.Result);
    }
  }
}
