﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Ninject;
using Prism.Ninject;

namespace CSharpRomanCalculator
{
  class Bootstrapper : NinjectBootstrapper
  {
    protected override void ConfigureKernel()
    {
      base.ConfigureKernel();

      Kernel.Bind<IStringNumToIntConverter>().To<RomanToInt>();
      Kernel.Bind<IStringNumberCalculator>().To<StringNumberCalculator>();
      Kernel.Bind<IConnectionLogFile>().To<ConnectionLogFile>();
      Kernel.Bind<IFileSystem>().To<FileSystem>();
      Kernel.Bind<ICalculationEntrySerializer>().To<CalculationEntrySerializer>();
      Kernel.Bind<IRepository<CalculationEntry>>().To<FileRepository>();
      Kernel.Bind<ICalculationEntryLogFactory>().To<CalculationEntryLogFactory>();
      Kernel.Bind<ICalculationEntryFactory>().To<CalculationEntryFactory>();
      Kernel.Bind<ICalculationEntryLog>().ToMethod(ctx =>
      {
        var log = ctx.Kernel.Get<ICalculationEntryLogFactory>().CreateLog();
        return log;
      })
      .InSingletonScope();


      Kernel.Bind<IExitTask>().To<SaveCalculationEntryLogExitTask>();
    }

    protected override void ConfigureModuleCatalog()
    {
    }
  }
}
