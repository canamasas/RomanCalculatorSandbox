﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using CSharpRomanCalculator.Annotations;

namespace CSharpRomanCalculator
{
  public class MainWindowVm : INotifyPropertyChanged
  {
    private ICommand _calculate;
    private ICommand _clearFields;
    private string _leftOperand;
    private string _result;
    private string _rightOperand;
    private readonly IStringNumberCalculator _calculator;
    private readonly ICalculationEntryLog _log;
    private readonly ICalculationEntryFactory _calculationEntryFactory;
    private string _errorMessage;

    public MainWindowVm(IStringNumberCalculator calculator, ICalculationEntryLog log,
      ICalculationEntryFactory calculationEntryFactory)
    {
      _log = log;
      _calculator = calculator;
      _calculationEntryFactory = calculationEntryFactory;
    }

    public string LeftOperand
    {
      get { return _leftOperand; }
      set
      {
        if (value != null)
        {
          _leftOperand = value;
          OnPropertyChanged(nameof(LeftOperand));
        }
      }
    }

    public string RightOperand
    {
      get { return _rightOperand; }
      set
      {
        if (value != null)
        {
          _rightOperand = value;
          OnPropertyChanged(nameof(RightOperand));
        }
      }
    }

    public string Result
    {
      get { return _result; }
      private set
      {
        if (value != null)
        {
          _result = value;
          OnPropertyChanged(nameof(Result));
        }
      }
    }

    public string ErrorMessage
    {
      get { return _errorMessage; }
      private set
      {
        if (value != null)
        {
          _errorMessage = value;
          OnPropertyChanged(nameof(ErrorMessage));
        }
      }
    }

    public ICommand ClearFieldsCommand
    {
      get
      {
        if (_clearFields == null)
          _clearFields = new RelayCommand(
            p => CanClearFields(),
            p => ClearFields());
        return _clearFields;
      }
    }

    public ICommand CalculateCommand
    {
      get
      {
        if (_calculate == null)
          _calculate = new RelayCommand(
            p => CanCalculate(),
            p => Calculate());
        return _calculate;
      }
    }

    public ICalculationEntryLog EntryLog => _log;

    //TODO: Must be an easier way of doing this!
    public List<Operation> Operations
      =>
      new List<Operation>()
      {
        new Operation() {Name = "+"},
        new Operation() {Name = "-"},
        new Operation() {Name = "*"},
        new Operation() {Name = "/"}
      };

    public string SelectedOperation { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    private void ClearFields()
    {
      LeftOperand = "";
      RightOperand = "";
      Result = "";
    }

    private bool CanClearFields()
    {
      return !string.IsNullOrEmpty(LeftOperand) || !string.IsNullOrEmpty(RightOperand) || !string.IsNullOrEmpty(Result);
    }

    private void Calculate()
    {
      try
      {
        Result = DoCalculation();
        var entry = _calculationEntryFactory.Create(LeftOperand, RightOperand, SelectedOperation, Result);
        EntryLog.Add(entry);
        ErrorMessage = "";
      }
      catch (InvalidCastException e)
      {
        ErrorMessage = e.Message;
      }
    }

    private string DoCalculation()
    {
      string result = "";
      if (SelectedOperation == "+")
      {
        result = _calculator.Add(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "-")
      {
        result = _calculator.Substract(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "*")
      {
        result = _calculator.Multiply(LeftOperand, RightOperand);
      }
      else if (SelectedOperation == "/")
      {
        result = _calculator.Divide(LeftOperand, RightOperand);
      }

      return result;
    }

    private bool CanCalculate()
    {
      if (!string.IsNullOrEmpty(RightOperand) && !string.IsNullOrEmpty(LeftOperand) && !string.IsNullOrEmpty(SelectedOperation))
      {
        return true;
      }
      return false;
    }

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}