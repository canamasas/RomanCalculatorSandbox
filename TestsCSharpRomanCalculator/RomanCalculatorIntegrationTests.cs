﻿using System;
using CSharpRomanCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsCSharpRomanCalculator
{
  [TestClass]
  public class RomanCalculatorIntegrationTests
  {
    private IStringNumberCalculator _romanNumberCalculator;

    [TestInitialize]
    public void Setup()
    {
      RomanToInt converter = new RomanToInt();
      _romanNumberCalculator = new StringNumberCalculator(converter);
    }

    [TestMethod]
    public void OnePlusOneIsTwo()
    {
      String result = _romanNumberCalculator.Add("I", "I");

      Assert.AreEqual("II", result);
    }

    [TestMethod]
    public void OnePlusTwoIsThree()
    {
      String result = _romanNumberCalculator.Add("I", "II");

      Assert.AreEqual("III", result);
    }

    [TestMethod]
    public void TenPlusTenIsTwenty()
    {
      String result = _romanNumberCalculator.Add("X", "X");

      Assert.AreEqual("XX", result);
    }

    [TestMethod]
    public void TwentyPlusTwentyIsForty()
    {
      String result = _romanNumberCalculator.Add("XX", "XX");

      Assert.AreEqual("XL", result);
    }
  }
}
