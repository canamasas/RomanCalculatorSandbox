﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Windows.Documents;
using System.Xml.Serialization;

namespace CSharpRomanCalculator
{
  public class CalculationEntrySerializer: ICalculationEntrySerializer
  {
    public void Serialize(Stream stream, IEnumerable<CalculationEntry> entries)
    {
      var collection = new CalculationEntryCollection(entries);
      var ser = new XmlSerializer(typeof(CalculationEntryCollection));
      ser.Serialize(stream, collection);
    }

    public IEnumerable<CalculationEntry> Deserialize(Stream stream)
    {
      var ser = new XmlSerializer(typeof(CalculationEntryCollection));
      var collection = (CalculationEntryCollection) ser.Deserialize(stream);

      return collection.List;
    }
  }
}